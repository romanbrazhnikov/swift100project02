//
//  ViewController.swift
//  Swift100Project2
//
//  Created by Roman Brazhnikov on 11/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var b_flagOne: UIButton!
    @IBOutlet var b_flagTwo: UIButton!
    @IBOutlet var b_flagThree: UIButton!
    
    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    let totalNumberOfQuestions = 5
    var questionsLeft: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionsLeft = totalNumberOfQuestions
        
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "uk", "us"]
        
        b_flagOne.layer.borderWidth = 1
        b_flagTwo.layer.borderWidth = 1
        b_flagThree.layer.borderWidth = 1
        
        b_flagOne.layer.borderColor = UIColor.lightGray.cgColor
        b_flagTwo.layer.borderColor = UIColor.lightGray.cgColor
        b_flagThree.layer.borderColor = UIColor.lightGray.cgColor
        
        
        askQuestion()
    }

    func askQuestion(action: UIAlertAction! = nil) {
        
        guard questionsLeft > 0 else {
            b_flagOne.isEnabled = false
            b_flagTwo.isEnabled = false
            b_flagThree.isEnabled = false
            
            let ac = UIAlertController(title: "GAME OVER", message: "Your final score is \(score)", preferredStyle: .alert)
            
            ac.addAction(UIAlertAction(title: "Finish", style: .default, handler: nil))
            
            present(ac, animated: true)
            title = "GAME OVER, SCORE: \(score)"
            return
        }
        questionsLeft -= 1
        
        countries.shuffle()
        
        correctAnswer = Int.random(in: 0...2)
        
        title = "Guess: " + countries[correctAnswer].uppercased() + ", Score: \(score)"
        
        b_flagOne.setImage(UIImage(named: countries[0]), for: .normal)
        b_flagTwo.setImage(UIImage(named: countries[1]), for: .normal)
        b_flagThree.setImage(UIImage(named: countries[2]), for: .normal)
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        var title: String
        var message: String
        
        if sender.tag == correctAnswer {
            title = "Correct"
            score += 1
            message = "Your score is \(score)"
        }
        else {
            title = "Wrong"
            score -= 1
            message = "That was \(countries[sender.tag].uppercased())\nYour score is \(score)"
        }
        
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestion))
        
        present(ac, animated: true)
    }
    
}

